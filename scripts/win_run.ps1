param($browser, $macro, $timeout = 3000)

###########################################################################
#Script to run Kantu macros and check on their result via the command line
###########################################################################

function PlayAndWait ([string]$macro, [string]$browserName, [Int]$timeout)
{

git reset --hard HEAD
git pull

$timeout_seconds = $timeout #max time in seconds allowed for macro to complete (change this value if  your macros takes longer to run)
$path_downloaddir = "c:\Users\Administrator\Downloads\" #where the kantu log file is stored ("downloaded") *THIS MUST BE THE BROWSER DOWNLOAD FOLDER*, as specified in the browser settings
$path_autorun_html = "c:\Users\Administrator\Downloads\ui.vision.html"

#Optional: Kill Chrome instances (if any open)
#taskkill /F /IM chrome.exe /T 

#Create log file. Here Kantu will store the result of the macro run
$log = "log_" + $(get-date -f MM-dd-yyyy_HH_mm_ss) + ".txt" 
$path_log = $path_downloaddir + $log 

#!!!!!!!!!!!!!!!!!!!
# Browser related
#!!!!!!!!!!!!!!!!!!!!

if ($browserName -eq "chrome") {

  $report_file_name="testreport_chrome.txt"
  $cmd = "C:\Program Files\Google\Chrome\Application\chrome.exe"
  
  if (!(Test-Path $cmd)) {
      $cmd = "C:\Users\Administrator\AppData\Local\Google\Chrome\Application\chrome.exe"
  }

} elseif ($browserName -eq "chrome_canary") {

  $report_file_name="testreport_chrome_canary.txt"
  $cmd = "C:\Users\Administrator\AppData\Local\Google\Chrome SxS\Application\chrome.exe"

} elseif ($browserName -eq "chrome_beta") {

  $report_file_name="testreport_chrome_beta.txt"
  $cmd = "C:\Program Files\Google\Chrome Beta\Application\chrome.exe"

  if (!(Test-Path $cmd)) {
    $cmd = "C:\Users\Administrator\AppData\Local\Google\Chrome Beta\Application\chrome.exe"
}

} elseif ($browserName -eq "firefox") {

  $report_file_name="testreport_firefox.txt"
  $cmd = "C:\Program Files\Mozilla Firefox\firefox.exe"

  if (!(Test-Path $cmd)) {
    $cmd = "C:\Program Files\Mozilla Firefox\firefox.exe"
  }

} else {

  throw "Invalid browser: " + $browserName

}

$arg = """file:///"+ $path_autorun_html + "?macro="+ $macro + "&storage=xfile&direct=1&closeBrowser=1&savelog="+$log+""""

Write-Host  "command line = " + $cmd
Write-Host  "command line arg = " + $arg

Start-Process -FilePath $cmd -ArgumentList $arg #Launch the browser and run the macro

#############Wait for macro to complete => Wait for log file to appear in download folder
$status_runtime = 0
Write-Host  "Log file will show up at " + $path_log
# while (!(Test-Path $path_log) -and ($status_runtime -lt $timeout_seconds)) 
while (($status_runtime -lt $timeout_seconds)) 
{ 
    Write-Host  "Waiting for macro to finish, seconds=" $status_runtime
    Start-Sleep 10
    $status_runtime = $status_runtime + 10

    $app = Get-Process | Where-Object {$_.Path -like $cmd}

    if (!$app) {
      break
    }
}


#Macro done - or timeout exceeded:
if ($status_runtime -lt $timeout_seconds)
{
    #Read FIRST line of log file, which contains the status of the last run
    $status_text = Get-Content $path_log -First 1


    #Check if macro completed OK or not
    $status_int = -1     
    If ($status_text -contains "Status=OK") {$status_int = 1}
}
else
{
    $status_text =  "Macro did not complete within the time given:" + $timeout_seconds
    $status_int = -2

    #Cleanup => Kill Chrome instance 
    Get-Process | Where-Object {$_.Path -like $cmd} | Stop-Process -Force

}

# remove-item $path_log #clean up

return $status_int, $status_text, $status_runtime
}


###########################################################################
#        Main program starts here
###########################################################################

function PlayLoop ([string]$macro, [string]$browserName, [Int]$timeout)
{

$testreport = "c:\Users\Administrator\Downloads\" + $report_file_name

while ($true)
{
    $i++

    $result = PlayAndWait $macro $browserName $timeout

    $errortext = $result[1] #Get error text or OK
    $runtime = $result[2] #Get runtime
    $report = $macro + " - Loop #" + $i + " runtime: ("+$runtime+" seconds), result: "+ $errortext

    Write-Host $report
    Add-content $testreport -value ($report)

    Start-Sleep 1
}

}

PlayLoop $macro $browser $timeout
